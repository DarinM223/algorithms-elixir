defmodule Algorithms.Graph.Node do
  alias Algorithms.Graph.Node

  defstruct data: nil, edges: []

  def bfs(id, storage, visited, f) do
    if Map.has_key?(visited, id) do
      []
    else
      visited = Map.put(visited, id, true)
      node = Map.get(storage, id)
      result = f.({id, node.data})
      child_results = node.edges |> Enum.flat_map(fn id -> bfs(id, storage, visited, f) end)
      [result | child_results]
    end
  end
end

defmodule Algorithms.Graph do
  alias Algorithms.Graph
  alias Algorithms.Graph.Node

  defstruct storage: %{}, curr_id: 0

  def add_node(%Graph{storage: storage, curr_id: curr_id}, node) do
    graph = %Graph{storage: Map.put(storage, curr_id, node), curr_id: curr_id + 1}
    {curr_id, graph}
  end

  def add_edge(graph, a, b) do
    storage = Map.update!(graph.storage, a, fn n -> %Node{n | edges: [b | n.edges]} end)
    %Graph{graph | storage: storage}
  end 

  def bfs(graph, id, f) do
    Node.bfs(id, graph.storage, %{}, f)
  end
end