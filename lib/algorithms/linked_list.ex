defmodule Algorithms.LinkedList.Node do
  @moduledoc """
  A node in a doubly linked list.

  A doubly linked list requires circular references so
  the next and prev "pointers" are simply ids to a map of
  id => node
  """
  alias Algorithms.LinkedList.Node

  defstruct data: nil, next: nil, prev: nil

  def print(%Node{data: nil}, storage), do: IO.puts("Nil")
  def print(%Node{data: data, next: nil}, storage) do
    IO.puts(inspect(data))
    IO.puts("Nil")
  end
  def print(%Node{data: data, next: next}, storage) do
    IO.puts(inspect(data))
    print(Map.get(storage, next), storage)
  end

  def reverse(id, storage) do
    case Map.get(storage, id) do
      %Node{data: data, next: nil} ->
        # Node without a next is the new tail node so null out its previous pointer.
        storage = Map.update!(storage, id, fn n -> %Node{n | prev: nil} end)
        {id, id, storage}
      %Node{data: data, next: next} ->
        {head, tail, storage} = reverse(next, storage)
        # Update both the tail and the new node in storage.
        storage =
          storage
          |> Map.update!(tail, fn n -> %Node{n | next: id} end)
          |> Map.update!(id, fn n -> %Node{n | next: nil, prev: tail} end)
        {head, id, storage}
    end
  end
end

defmodule Algorithms.LinkedList do
  @moduledoc """
  A doubly linked list. It uses a map of ids to nodes to keep track
  of the pointers.

  ## Example

      iex> alias Algorithms.LinkedList
      Algorithms.LinkedList
      iex> list = %LinkedList{}
      %Algorithms.LinkedList{curr_id: 0, head: nil, storage: %{}, tail: nil}
      iex> node = %LinkedList.Node{data: 2}
      %Algorithms.LinkedList.Node{data: 2, next: nil, prev: nil}
      iex> list |> LinkedList.add_node(node) |> LinkedList.print
      2
      Nil
      :ok
      iex> list |> LinkedList.add_node(node) |> LinkedList.add_node(node) |> LinkedList.print
      2
      2
      Nil
      :ok

  """

  alias Algorithms.LinkedList
  alias Algorithms.LinkedList.Node

  defstruct storage: %{}, head: nil, tail: nil, curr_id: 0

  def add_node(%LinkedList{head: nil, storage: storage, curr_id: curr_id} = list, node) do
    %LinkedList{list | storage: Map.put(storage, curr_id, node),
                       head: curr_id,
                       tail: curr_id,
                       curr_id: curr_id + 1}
  end
  def add_node(list, node) do
    # Set node's previous pointer to the list's tail node.'
    node = %Node{node | prev: list.tail}

    # Add node to storage and set the tail node's next pointer
    # to the new node.
    storage =
      list.storage
      |> Map.put(list.curr_id, node)
      |> Map.update!(list.tail, fn n -> %Node{n | next: list.curr_id} end)

    # Set the new tail node to the added node.
    %LinkedList{list | storage: storage,
                       tail: list.curr_id,
                       curr_id: list.curr_id + 1}
  end

  def print(%LinkedList{head: nil}), do: IO.puts("Nil")
  def print(%LinkedList{head: head, storage: storage}) do
    Node.print(Map.get(storage, head), storage)
  end
  
  def reverse(list) do
    {head, tail, storage} = Node.reverse(list.head, list.storage)
    %LinkedList{list | storage: storage, head: head, tail: tail}
  end
end